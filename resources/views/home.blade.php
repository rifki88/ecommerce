@extends('layouts.app')

	<script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="VT-client-L0mYJ4NTiCJl039g"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    
	
@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                  
				 
				<table width="100%">
				<tr>	
					<td>
				<form id="payment-form" method="post" action="snapfinish">
			      <input type="hidden" name="_token" value="{!! csrf_token() !!}">
			      <input type="hidden" name="result_type" id="result-type" value=""></div>
			      <input type="hidden" name="result_data" id="result-data" value=""></div>
			    </form>
				
				
				<h2>Top Up</h2>
			    <p>
				<input type="text" name="topup" id="topup" style="width:90%;">
				</p>
				<p>
				<input type="button" id="pay-button" value="send">
				</p>
				
				</td><td>
				

				<h2>Pencairan</h2>
				<form method="post" action="/pencairan">
				<input type="hidden" name="_token" value="{!! csrf_token() !!}">
			    <p>
				<input type="text" name="pencairan" id="pencairan" style="width:90%;">
				</p>
				<p>
				<input type="submit" id="pencairan" value="send">
				</p>
				</form>

					</td></tr>
					
					</table>
					
					<div class="table-responsive">          
		  		
					<table class="table">
						<tr>
							<th>ID</th>
							<th>Debit</th>
							<th>Kredit</th>
							<th>Saldo</th>
							<th>#</th>
						</tr>
						
						@foreach($data as $row)
						<tr>
							<td>{{$row->id}}</td>
							<td>{{$row->debit}}</td>
							<td>{{$row->credit}}</td>
							<td>{{$row->saldo}}</td>
							<td><a href="/delete/{{$row->id}}">X</a></td>
						</tr>
						@endforeach
					</table>
					</div>
					
					
						   </div>	
						</div>
		            </div>
					
		        </div>
				
				
		    </div>
</div>






<script type="text/javascript">
  
    $('#pay-button').click(function (event) {
      event.preventDefault();
      $(this).attr("disabled", "disabled");
	  var a = document.getElementById('topup').value;
    $.ajax({
	  
      url: './snaptoken/'+a,
      cache: false,

      success: function(data) {
        //location = data;

        console.log('token = '+data);
        
        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');

        function changeResult(type,data){
          $("#result-type").val(type);
          $("#result-data").val(JSON.stringify(data));
          //resultType.innerHTML = type;
          //resultData.innerHTML = JSON.stringify(data);
        }

        snap.pay(data, {
          
          onSuccess: function(result){
            changeResult('success', result);
            console.log(result.status_message);
            console.log(result);
            $("#payment-form").submit();
          },
          onPending: function(result){
            changeResult('pending', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          },
          onError: function(result){
            changeResult('error', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          }
        });
      }
    });
  });

  </script>
	
	
	
@endsection
