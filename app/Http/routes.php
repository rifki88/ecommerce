<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/delete/{id}', 'SnapController@delete');

Route::post('/pencairan', 'SnapController@pencairan');

Route::get('/vtweb', 'vtwebController@vtweb');

Route::get('/vtdirect', 'PagesController@vtdirect');
Route::post('/vtdirect', 'PagesController@checkout_process');

Route::get('/vt_transaction', 'PagesController@transaction');
Route::post('/vt_transaction', 'PagesController@transaction_process');

Route::post('/vt_notif', 'PagesController@notification');

Route::get('/snap', 'SnapController@snap');
//Route::get('/snaptoken', 'SnapController@token');
Route::get('/snaptoken/{id}', 'SnapController@token');
Route::post('/snapfinish', 'SnapController@finish');
