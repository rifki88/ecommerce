<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Veritrans\Midtrans;
use App\Veritrans\Veritrans;
use DB;
use Auth;

class SnapController extends Controller
{
    public function __construct()
    {   
        Midtrans::$serverKey = 'VT-server-fNguoTpCTngkOIWdKTFJXd59';
        Midtrans::$isProduction = false;
    }

    public function snap()
    {
        return view('snap_checkout');
    }

    public function token($id) 
    {
        error_log('masuk ke snap token adri ajax');
        $midtrans = new Midtrans;
		
		
		//$id =  Session::get('nama');
				
        $transaction_details = array(
            'order_id'          => uniqid(),
            'gross_amount'  => $id
        );

        
        $customer_details = array(
            'first_name'            => Auth::user()->name,
            'email'                     => Auth::user()->email,
            'phone'                     => "081322311801"
           
            );

        // Data yang akan dikirim untuk request redirect_url.
        // Uncomment 'credit_card_3d_secure' => true jika transaksi ingin diproses dengan 3DSecure.
        $transaction_data = array(
            'transaction_details'=> $transaction_details,
         
            'customer_details'   => $customer_details
        );
    
        try
        {
            $snap_token = $midtrans->getSnapToken($transaction_data);
            //return redirect($vtweb_url);
            echo $snap_token;
        } 
        catch (Exception $e) 
        {   
            return $e->getMessage;
        }
    }

    public function finish(Request $request)
    {
        $result = $request->input('result_data');
        $result = json_decode($result);
        //echo $result->status_message . '<br>';
        //echo 'RESULT <br><pre>';
        //print_r($result);
        //echo '</pre>' ;
		
		$sql = DB::select("select * from transaksis");
		$cek = count($sql);
		if($cek == 0){
		DB::select("insert into transaksis(credit,saldo,id_user)values('".$result->gross_amount."','".$result->gross_amount."','".Auth::User()->id."')");
		}else{
		$query = DB::select("select * from transaksis order by id DESC limit 1");
		DB::select("insert into transaksis(credit,saldo,id_user)values('".$result->gross_amount."','".($result->gross_amount+$query[0]->saldo)."','".Auth::User()->id."')");
		}
		
		return redirect('/home');
    }

	public function pencairan(Request $request){
	
	$data = DB::select("select * from transaksis order by id DESC limit 1");
	DB::select("insert into transaksis(debit,saldo,id_user)values('".$request->pencairan."','".($data[0]->saldo-$request->pencairan)."','".Auth::User()->id."')");
	return redirect('home');
	
	}
	
	
	public function delete($id){
	
		$delete = Transaksi::findOrFail($id);
		$delete->delete();
		return redirect('/home');
	
	}
	
    public function notification()
    {
        $midtrans = new Midtrans;
        echo 'test notification handler';
        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);

        if($result){
        $notif = $midtrans->status($result->order_id);
        }

        error_log(print_r($result,TRUE));

        /*
        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status;

        if ($transaction == 'capture') {
          // For credit card transaction, we need to check whether transaction is challenge by FDS or not
          if ($type == 'credit_card'){
            if($fraud == 'challenge'){
              // TODO set payment status in merchant's database to 'Challenge by FDS'
              // TODO merchant should decide whether this transaction is authorized or not in MAP
              echo "Transaction order_id: " . $order_id ." is challenged by FDS";
              } 
              else {
              // TODO set payment status in merchant's database to 'Success'
              echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
              }
            }
          }
        else if ($transaction == 'settlement'){
          // TODO set payment status in merchant's database to 'Settlement'
          echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
          } 
          else if($transaction == 'pending'){
          // TODO set payment status in merchant's database to 'Pending'
          echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
          } 
          else if ($transaction == 'deny') {
          // TODO set payment status in merchant's database to 'Denied'
          echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
        }*/
   
    }
}    